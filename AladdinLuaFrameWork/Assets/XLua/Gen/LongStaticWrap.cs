﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class LongStaticWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(LongStatic), L, translator, 0, 0, 0, 0);
			
			
			
			
			
			Utils.EndObjectRegister(typeof(LongStatic), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(LongStatic), L, __CreateInstance, 1, 2, 2);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(LongStatic));
			Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "LONG_MAX", _g_get_LONG_MAX);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "ULONG_MAX", _g_get_ULONG_MAX);
            
			Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "LONG_MAX", _s_set_LONG_MAX);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "ULONG_MAX", _s_set_ULONG_MAX);
            
			Utils.EndClassRegister(typeof(LongStatic), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					LongStatic __cl_gen_ret = new LongStatic();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to LongStatic constructor!");
            
        }
        
		
        
		
        
        
        
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_LONG_MAX(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushint64(L, LongStatic.LONG_MAX);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_ULONG_MAX(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushuint64(L, LongStatic.ULONG_MAX);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_LONG_MAX(RealStatePtr L)
        {
            
            try {
			    LongStatic.LONG_MAX = LuaAPI.lua_toint64(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_ULONG_MAX(RealStatePtr L)
        {
            
            try {
			    LongStatic.ULONG_MAX = LuaAPI.lua_touint64(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
