﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class LuaTestCommonWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(LuaTestCommon), L, translator, 0, 0, 0, 0);
			
			
			
			
			
			Utils.EndObjectRegister(typeof(LuaTestCommon), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(LuaTestCommon), L, __CreateInstance, 3, 4, 4);
			Utils.RegisterFunc(L, Utils.CLS_IDX, "IsMacPlatform", _m_IsMacPlatform_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "IsIOSPlatform", _m_IsIOSPlatform_xlua_st_);
            
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(LuaTestCommon));
			Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "resultPath", _g_get_resultPath);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "xxxtdrfilepath", _g_get_xxxtdrfilepath);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "xxxtdr2filepath", _g_get_xxxtdr2filepath);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "android_platform", _g_get_android_platform);
            
			Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "resultPath", _s_set_resultPath);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "xxxtdrfilepath", _s_set_xxxtdrfilepath);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "xxxtdr2filepath", _s_set_xxxtdr2filepath);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "android_platform", _s_set_android_platform);
            
			Utils.EndClassRegister(typeof(LuaTestCommon), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					LuaTestCommon __cl_gen_ret = new LuaTestCommon();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to LuaTestCommon constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_IsMacPlatform_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    
                        bool __cl_gen_ret = LuaTestCommon.IsMacPlatform(  );
                        LuaAPI.lua_pushboolean(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_IsIOSPlatform_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    
                        bool __cl_gen_ret = LuaTestCommon.IsIOSPlatform(  );
                        LuaAPI.lua_pushboolean(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_resultPath(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushstring(L, LuaTestCommon.resultPath);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_xxxtdrfilepath(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushstring(L, LuaTestCommon.xxxtdrfilepath);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_xxxtdr2filepath(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushstring(L, LuaTestCommon.xxxtdr2filepath);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_android_platform(RealStatePtr L)
        {
            
            try {
			    LuaAPI.lua_pushboolean(L, LuaTestCommon.android_platform);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_resultPath(RealStatePtr L)
        {
            
            try {
			    LuaTestCommon.resultPath = LuaAPI.lua_tostring(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_xxxtdrfilepath(RealStatePtr L)
        {
            
            try {
			    LuaTestCommon.xxxtdrfilepath = LuaAPI.lua_tostring(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_xxxtdr2filepath(RealStatePtr L)
        {
            
            try {
			    LuaTestCommon.xxxtdr2filepath = LuaAPI.lua_tostring(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_android_platform(RealStatePtr L)
        {
            
            try {
			    LuaTestCommon.android_platform = LuaAPI.lua_toboolean(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
