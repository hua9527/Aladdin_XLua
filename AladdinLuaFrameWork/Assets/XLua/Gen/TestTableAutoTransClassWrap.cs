﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class TestTableAutoTransClassWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(TestTableAutoTransClass), L, translator, 0, 6, 0, 0);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "SimpleClassMethod", _m_SimpleClassMethod);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "ComplexClassMethod", _m_ComplexClassMethod);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "SimpleStructMethod", _m_SimpleStructMethod);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "ComplexStructMethod", _m_ComplexStructMethod);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "OneListMethod", _m_OneListMethod);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "TwoDimensionListMethod", _m_TwoDimensionListMethod);
			
			
			
			
			Utils.EndObjectRegister(typeof(TestTableAutoTransClass), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(TestTableAutoTransClass), L, __CreateInstance, 1, 0, 0);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(TestTableAutoTransClass));
			
			
			Utils.EndClassRegister(typeof(TestTableAutoTransClass), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					TestTableAutoTransClass __cl_gen_ret = new TestTableAutoTransClass();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to TestTableAutoTransClass constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SimpleClassMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TableAutoTransSimpleClass p = (TableAutoTransSimpleClass)translator.GetObject(L, 2, typeof(TableAutoTransSimpleClass));
                    
                        TableAutoTransSimpleClass __cl_gen_ret = __cl_gen_to_be_invoked.SimpleClassMethod( p );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ComplexClassMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TableAutoTransComplexClass p = (TableAutoTransComplexClass)translator.GetObject(L, 2, typeof(TableAutoTransComplexClass));
                    
                        TableAutoTransComplexClass __cl_gen_ret = __cl_gen_to_be_invoked.ComplexClassMethod( p );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SimpleStructMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TableAutoTransSimpleStruct p;translator.Get(L, 2, out p);
                    
                        TableAutoTransSimpleStruct __cl_gen_ret = __cl_gen_to_be_invoked.SimpleStructMethod( p );
                        translator.PushTableAutoTransSimpleStruct(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ComplexStructMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TableAutoTransComplexStruct p;translator.Get(L, 2, out p);
                    
                        TableAutoTransComplexStruct __cl_gen_ret = __cl_gen_to_be_invoked.ComplexStructMethod( p );
                        translator.PushTableAutoTransComplexStruct(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_OneListMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    int[] args = (int[])translator.GetObject(L, 2, typeof(int[]));
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.OneListMethod( args );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_TwoDimensionListMethod(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestTableAutoTransClass __cl_gen_to_be_invoked = (TestTableAutoTransClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    int[][] args = (int[][])translator.GetObject(L, 2, typeof(int[][]));
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.TwoDimensionListMethod( args );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        
        
		
		
		
		
    }
}
