﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class CClassWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(CClass), L, translator, 0, 2, 1, 1);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Add", _m_Add);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "VariableParamFunc", _m_VariableParamFunc);
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "CConStruct", _g_get_CConStruct);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "CConStruct", _s_set_CConStruct);
            
			Utils.EndObjectRegister(typeof(CClass), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(CClass), L, __CreateInstance, 1, 0, 0);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(CClass));
			
			
			Utils.EndClassRegister(typeof(CClass), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 4 && LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 2) && LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 3) && (LuaAPI.lua_isnil(L, 4) || LuaAPI.lua_type(L, 4) == LuaTypes.LUA_TSTRING))
				{
					int x = LuaAPI.xlua_tointeger(L, 2);
					int y = LuaAPI.xlua_tointeger(L, 3);
					string z = LuaAPI.lua_tostring(L, 4);
					
					CClass __cl_gen_ret = new CClass(x, y, z);
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to CClass constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Add(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            CClass __cl_gen_to_be_invoked = (CClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    HasConstructStruct invar;translator.Get(L, 2, out invar);
                    HasConstructStruct refvar;translator.Get(L, 3, out refvar);
                    HasConstructStruct outvar;
                    
                    __cl_gen_to_be_invoked.Add( invar, ref refvar, out outvar );
                    translator.Push(L, refvar);
                        translator.Update(L, 3, refvar);
                        
                    translator.Push(L, outvar);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_VariableParamFunc(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            CClass __cl_gen_to_be_invoked = (CClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    HasConstructStruct[] structs = translator.GetParams<HasConstructStruct>(L, 2);
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.VariableParamFunc( structs );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_CConStruct(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                CClass __cl_gen_to_be_invoked = (CClass)translator.FastGetCSObj(L, 1);
                translator.Push(L, __cl_gen_to_be_invoked.CConStruct);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_CConStruct(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                CClass __cl_gen_to_be_invoked = (CClass)translator.FastGetCSObj(L, 1);
                HasConstructStruct __cl_gen_value;translator.Get(L, 2, out __cl_gen_value);
				__cl_gen_to_be_invoked.CConStruct = __cl_gen_value;
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
