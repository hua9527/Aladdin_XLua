﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;
using TestExtensionMethod;

namespace CSObjectWrap
{
    public class TestChineseStringWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(TestChineseString), L, translator, 0, 9, 5, 5);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetShortChinString", _m_GetShortChinString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetLongChineString", _m_GetLongChineString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetCombineString", _m_GetCombineString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetComplexString", _m_GetComplexString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetHuoxingString", _m_GetHuoxingString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "PrintAllString", _m_PrintAllString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetLongStringLength", _m_GetLongStringLength);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Add", _m_Add);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Replace", _m_Replace);
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "short_simple_string", _g_get_short_simple_string);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "long_simple_string", _g_get_long_simple_string);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "combine_string", _g_get_combine_string);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "complex_string", _g_get_complex_string);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "huoxing_string", _g_get_huoxing_string);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "short_simple_string", _s_set_short_simple_string);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "long_simple_string", _s_set_long_simple_string);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "combine_string", _s_set_combine_string);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "complex_string", _s_set_complex_string);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "huoxing_string", _s_set_huoxing_string);
            
			Utils.EndObjectRegister(typeof(TestChineseString), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(TestChineseString), L, __CreateInstance, 1, 0, 0);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(TestChineseString));
			
			
			Utils.EndClassRegister(typeof(TestChineseString), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					TestChineseString __cl_gen_ret = new TestChineseString();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to TestChineseString constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetShortChinString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.GetShortChinString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetLongChineString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.GetLongChineString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetCombineString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.GetCombineString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetComplexString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.GetComplexString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetHuoxingString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.GetHuoxingString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_PrintAllString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.PrintAllString(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetLongStringLength(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.GetLongStringLength(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Add(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TestChineseString d = (TestChineseString)translator.GetObject(L, 2, typeof(TestChineseString));
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.Add( d );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Replace(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    TestChineseString d = (TestChineseString)translator.GetObject(L, 2, typeof(TestChineseString));
                    TestChineseString e;
                    TestChineseString a = (TestChineseString)translator.GetObject(L, 3, typeof(TestChineseString));
                    
                    __cl_gen_to_be_invoked.Replace( d, out e, ref a );
                    translator.Push(L, e);
                        
                    translator.Push(L, a);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_short_simple_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                LuaAPI.lua_pushstring(L, __cl_gen_to_be_invoked.short_simple_string);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_long_simple_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                LuaAPI.lua_pushstring(L, __cl_gen_to_be_invoked.long_simple_string);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_combine_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                LuaAPI.lua_pushstring(L, __cl_gen_to_be_invoked.combine_string);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_complex_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                LuaAPI.lua_pushstring(L, __cl_gen_to_be_invoked.complex_string);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_huoxing_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                LuaAPI.lua_pushstring(L, __cl_gen_to_be_invoked.huoxing_string);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_short_simple_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.short_simple_string = LuaAPI.lua_tostring(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_long_simple_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.long_simple_string = LuaAPI.lua_tostring(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_combine_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.combine_string = LuaAPI.lua_tostring(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_complex_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.complex_string = LuaAPI.lua_tostring(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_huoxing_string(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TestChineseString __cl_gen_to_be_invoked = (TestChineseString)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.huoxing_string = LuaAPI.lua_tostring(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
