﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class GenCodeBaseClassWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(GenCodeBaseClass), L, translator, 0, 4, 1, 1);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "InitStruct", _m_InitStruct);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "SetStruct", _m_SetStruct);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "GetStruct", _m_GetStruct);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "SetStaticPusherStruct", _m_SetStaticPusherStruct);
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "static_pushstruct_var", _g_get_static_pushstruct_var);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "static_pushstruct_var", _s_set_static_pushstruct_var);
            
			Utils.EndObjectRegister(typeof(GenCodeBaseClass), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(GenCodeBaseClass), L, __CreateInstance, 1, 3, 3);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(GenCodeBaseClass));
			Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "struct_var1", _g_get_struct_var1);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "struct_var2", _g_get_struct_var2);
            Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "GBS", _g_get_GBS);
            
			Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "struct_var1", _s_set_struct_var1);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "struct_var2", _s_set_struct_var2);
            Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "GBS", _s_set_GBS);
            
			Utils.EndClassRegister(typeof(GenCodeBaseClass), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					GenCodeBaseClass __cl_gen_ret = new GenCodeBaseClass();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to GenCodeBaseClass constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_InitStruct(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    int add;
                    
                        GenCodeStruct __cl_gen_ret = __cl_gen_to_be_invoked.InitStruct( out add );
                        translator.Push(L, __cl_gen_ret);
                    LuaAPI.xlua_pushinteger(L, add);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetStruct(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
            
            
			int __gen_param_count = LuaAPI.lua_gettop(L);
            
            try {
                if(__gen_param_count == 2&& translator.Assignable<GenCodeStruct>(L, 2)) 
                {
                    GenCodeStruct var;translator.Get(L, 2, out var);
                    GenCodeStruct value;
                    
                    __cl_gen_to_be_invoked.SetStruct( ref var, out value );
                    translator.Push(L, var);
                        translator.Update(L, 2, var);
                        
                    translator.Push(L, value);
                        
                    
                    
                    
                    return 2;
                }
                if(__gen_param_count == 2&& translator.Assignable<NoGenCodeStruct>(L, 2)) 
                {
                    NoGenCodeStruct var;translator.Get(L, 2, out var);
                    int add;
                    GenCodeStruct value;
                    
                    __cl_gen_to_be_invoked.SetStruct( var, out add, out value );
                    LuaAPI.xlua_pushinteger(L, add);
                        
                    translator.Push(L, value);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to GenCodeBaseClass.SetStruct!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetStruct(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        GenCodeStruct __cl_gen_ret = __cl_gen_to_be_invoked.GetStruct(  );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetStaticPusherStruct(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    StaticPusherStructAll inVar;translator.Get(L, 2, out inVar);
                    StaticPusherStructAll refVar;translator.Get(L, 3, out refVar);
                    StaticPusherStructAll outVar;
                    
                    __cl_gen_to_be_invoked.SetStaticPusherStruct( inVar, ref refVar, out outVar );
                    translator.PushStaticPusherStructAll(L, refVar);
                        translator.UpdateStaticPusherStructAll(L, 3, refVar);
                        
                    translator.PushStaticPusherStructAll(L, outVar);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_static_pushstruct_var(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
                translator.PushStaticPusherStructAll(L, __cl_gen_to_be_invoked.static_pushstruct_var);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_struct_var1(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			    translator.Push(L, GenCodeBaseClass.struct_var1);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_struct_var2(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			    translator.Push(L, GenCodeBaseClass.struct_var2);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_GBS(RealStatePtr L)
        {
            
            try {
			    LuaAPI.xlua_pushinteger(L, GenCodeBaseClass.GBS);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_static_pushstruct_var(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                GenCodeBaseClass __cl_gen_to_be_invoked = (GenCodeBaseClass)translator.FastGetCSObj(L, 1);
                StaticPusherStructAll __cl_gen_value;translator.Get(L, 2, out __cl_gen_value);
				__cl_gen_to_be_invoked.static_pushstruct_var = __cl_gen_value;
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_struct_var1(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			GenCodeStruct __cl_gen_value;translator.Get(L, 1, out __cl_gen_value);
				GenCodeBaseClass.struct_var1 = __cl_gen_value;
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_struct_var2(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			HasConstructStruct __cl_gen_value;translator.Get(L, 1, out __cl_gen_value);
				GenCodeBaseClass.struct_var2 = __cl_gen_value;
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_GBS(RealStatePtr L)
        {
            
            try {
			    GenCodeBaseClass.GBS = LuaAPI.xlua_tointeger(L, 1);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
