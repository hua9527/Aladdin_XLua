﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class GenCodeDrivedClassWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(GenCodeDrivedClass), L, translator, 0, 1, 1, 1);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "Add", _m_Add);
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "SimpleConStruct", _g_get_SimpleConStruct);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "SimpleConStruct", _s_set_SimpleConStruct);
            
			Utils.EndObjectRegister(typeof(GenCodeDrivedClass), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(GenCodeDrivedClass), L, __CreateInstance, 1, 0, 0);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(GenCodeDrivedClass));
			
			
			Utils.EndClassRegister(typeof(GenCodeDrivedClass), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					GenCodeDrivedClass __cl_gen_ret = new GenCodeDrivedClass();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to GenCodeDrivedClass constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Add(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            GenCodeDrivedClass __cl_gen_to_be_invoked = (GenCodeDrivedClass)translator.FastGetCSObj(L, 1);
            
            
			int __gen_param_count = LuaAPI.lua_gettop(L);
            
            try {
                if(__gen_param_count == 3&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 2)&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 3)) 
                {
                    int x = LuaAPI.xlua_tointeger(L, 2);
                    int y = LuaAPI.xlua_tointeger(L, 3);
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.Add( x, y );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                if(__gen_param_count == 3&& translator.Assignable<HasConstructStruct>(L, 2)&& translator.Assignable<HasConstructStruct>(L, 3)) 
                {
                    HasConstructStruct invar;translator.Get(L, 2, out invar);
                    HasConstructStruct refvar;translator.Get(L, 3, out refvar);
                    HasConstructStruct outvar;
                    
                    __cl_gen_to_be_invoked.Add( invar, ref refvar, out outvar );
                    translator.Push(L, refvar);
                        translator.Update(L, 3, refvar);
                        
                    translator.Push(L, outvar);
                        
                    
                    
                    
                    return 2;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to GenCodeDrivedClass.Add!");
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_SimpleConStruct(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                GenCodeDrivedClass __cl_gen_to_be_invoked = (GenCodeDrivedClass)translator.FastGetCSObj(L, 1);
                translator.Push(L, __cl_gen_to_be_invoked.SimpleConStruct);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_SimpleConStruct(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                GenCodeDrivedClass __cl_gen_to_be_invoked = (GenCodeDrivedClass)translator.FastGetCSObj(L, 1);
                HasConstructStruct __cl_gen_value;translator.Get(L, 2, out __cl_gen_value);
				__cl_gen_to_be_invoked.SimpleConStruct = __cl_gen_value;
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
