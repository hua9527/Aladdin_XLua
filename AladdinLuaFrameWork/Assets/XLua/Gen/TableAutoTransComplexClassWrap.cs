﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace CSObjectWrap
{
    public class TableAutoTransComplexClassWrap
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(TableAutoTransComplexClass), L, translator, 0, 0, 4, 4);
			
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "IntVar", _g_get_IntVar);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "ClassVar", _g_get_ClassVar);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "A", _g_get_A);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "B", _g_get_B);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "IntVar", _s_set_IntVar);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "ClassVar", _s_set_ClassVar);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "A", _s_set_A);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "B", _s_set_B);
            
			Utils.EndObjectRegister(typeof(TableAutoTransComplexClass), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(TableAutoTransComplexClass), L, __CreateInstance, 1, 0, 0);
			
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(TableAutoTransComplexClass));
			
			
			Utils.EndClassRegister(typeof(TableAutoTransComplexClass), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					TableAutoTransComplexClass __cl_gen_ret = new TableAutoTransComplexClass();
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				if(LuaAPI.lua_gettop(L) == 3 && LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 2) && translator.Assignable<TableAutoTransSimpleClass>(L, 3))
				{
					int A = LuaAPI.xlua_tointeger(L, 2);
					TableAutoTransSimpleClass B = (TableAutoTransSimpleClass)translator.GetObject(L, 3, typeof(TableAutoTransSimpleClass));
					
					TableAutoTransComplexClass __cl_gen_ret = new TableAutoTransComplexClass(A, B);
					translator.Push(L, __cl_gen_ret);
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to TableAutoTransComplexClass constructor!");
            
        }
        
		
        
		
        
        
        
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_IntVar(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                LuaAPI.xlua_pushinteger(L, __cl_gen_to_be_invoked.IntVar);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_ClassVar(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                translator.Push(L, __cl_gen_to_be_invoked.ClassVar);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_A(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                LuaAPI.xlua_pushinteger(L, __cl_gen_to_be_invoked.A);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_B(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                translator.Push(L, __cl_gen_to_be_invoked.B);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_IntVar(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.IntVar = LuaAPI.xlua_tointeger(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_ClassVar(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.ClassVar = (TableAutoTransSimpleClass)translator.GetObject(L, 2, typeof(TableAutoTransSimpleClass));
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_A(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.A = LuaAPI.xlua_tointeger(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_B(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                TableAutoTransComplexClass __cl_gen_to_be_invoked = (TableAutoTransComplexClass)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.B = (TableAutoTransSimpleClass)translator.GetObject(L, 2, typeof(TableAutoTransSimpleClass));
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
